-- JI
-- 21 februari 2018
--

USE ModernWays;

DROP TABLE IF EXISTS `Postcodes`;

-- de naam van de tabel in Pascalnotatie
CREATE TABLE Postcodes(
    Code CHAR(4),
    Plaats NVARCHAR(50),
    Localite NVARCHAR(50),
    Provincie NVARCHAR(50),
    Provence NVARCHAR(50)
);

